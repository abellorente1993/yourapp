package com.example.yourapp.controller;

import com.example.yourapp.api.DefaultApi;
import com.example.yourapp.model.ProductDetail;
import com.example.yourapp.client.SimilarProductClient;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductsController implements DefaultApi {

    @Autowired
    private SimilarProductClient similarProductClient;

    @GetMapping("/product/{productId}/similar")
    public ResponseEntity<List<ProductDetail>> getSimilarProducts(@PathVariable String productId) {

        return ResponseEntity.ok(
                similarProductClient.getSimilarProductIds(productId)
                        .stream()
                        .map(similarProductClient::getProductById)
                        .collect(Collectors.toList())
        );
    }

    @RestControllerAdvice
    public class GlobalExceptionHandler {

        @ExceptionHandler(FeignException.class)
        public String handleFeignStatusException(FeignException e, HttpServletResponse response) {
            response.setStatus(e.status());
            return "Something was wrong, the product can't be found or the parameters are incorrects.";
        }

    }

}
