package com.example.yourapp.client;

import com.example.yourapp.model.ProductDetail;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "SimilarProductClient", url = "localhost:3001")
public interface SimilarProductClient {

    @GetMapping(value = "/product/{productId}")
    ProductDetail getProductById(@PathVariable String productId);

    @GetMapping(value = "/product/{productId}/similarids")
    List<String> getSimilarProductIds(@PathVariable String productId);

}
