# Backend dev technical test
Se desarrolla la solución indicada haciendo uso de una aplicación Java + SpringBoot y utilizando Maven.
Al arrancar la aplicación si queremos visualizar con Swagger el endpoint expuesto, podemos conectarnos con la siguiente url:

 [http://localhost:5000/similarProducts.html](http://localhost:5000/similarProducts.html)
 
Con esto se mostrará la pantalla:
![Diagram](./assets/swagger1.jpg "Diagram")


Por otro lado, antes de lanzar la batería de test que se nos adjunta con el docker-compose, se prueba a ejecutar un ejemplo:

![Diagram](./assets/swagger2.jpg "Diagram")

## Testing

Se lanzan los test con el siguiente comando:
```
docker-compose run --rm k6 run scripts/test.js
```

El resultado es el siguiente

![Diagram](./assets/testResult.jpg "Diagram")

Se observan también los resultados en grafana:

![Diagram](./assets/grafana.png "Diagram")